const express = require("express");
const hbs = require("hbs");
const path = require("path");
const app = express();
const weatherData = require("../utils/weatherData");

const port = process.env.PORT || 3000;
const publicStaticDirPath = path.join(__dirname, "../public");
const viewsPath = path.join(__dirname, "../templates/views");
const partialsPath = path.join(__dirname, "../templates/partials");

app.set("view engine", "hbs");
app.set("views", viewsPath);
hbs.registerPartials(partialsPath);
app.use(express.static(publicStaticDirPath));

app.get("", (req, res) => {
  res.render("index", {
    title: "SmartCodes Weather",
  });
});

app.get("/weather", (req, res) => {
  const region = req.query.region;
  if (!region) return res.send({ error: "Region cannot be empty" });
  weatherData(
    region,
    (
      error,
      {
        temperature,
        description,
        cityName,
        icon,
        wind,
        humidity,
        pressure,
        des,
      }
    ) => {
      if (error) return res.send(error);
      return res.send({
        temperature,
        description,
        cityName,
        icon,
        wind,
        humidity,
        pressure,
        des,
      });
    }
  );
});

app.get("*", (req, res) => {
  res.render("404", { title: "page not found" });
});

app.listen(port, () => {
  console.log(`Server started on port: ${port}`);
});
